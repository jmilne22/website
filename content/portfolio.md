+++
date = "2017-01-25T16:35:52-05:00"
slug = ""
comments = false
draft = false
showcomments = false
title = "portfolio"
categories = []
tags = []
showpagemeta = true

+++


> **RamsayBot: An IRC Bot written in Java**
>
> RamsayBot is an IRC bot named after Gordon Ramsay written in Java using the
> PircBot API that can relay Gordon Ramsay quotes and frequent links on command.
> It also uses a Java library for OpenWeatherMap that can grab the weather,
> humidity for any location and displays it in celcius and fahrenheit.
>
> * [Source Code](https://github.com/jmilne22/RamsayBot)
> * [PircBot API](http://www.jibble.org/pircbot.php)
> * [OpenWeatherMap Java Library](https://bitbucket.org/akapribot/owm-japis/overview)
>
<br>
> **CSV to database: A CSV parser written in Java**
>
> CSV to database is a web application written in Java using servlets, JSP and
> Expression language to display the data to the user. The user uploads a CSV
> using a web form and the application parses the file, stores it in a MYSQL
> database and displays it back to the user in a table.
>
> * [Source Code](https://github.com/jmilne22/Csv2Database)
>
<br>
> **JavaFX Clock Widget**
>
> A clock widget for Windows, Mac and Linux desktops written using JavaFX and
> styled with CSS
>
> * [Source Code](https://github.com/jmilne22/Csv2Database)
> * [Screenshot](https://i.imgur.com/vUPB2l8.png)
